package patrykdadas.model.flights.results.filters;

import static com.codeborne.selenide.Selenide.$;

import java.util.HashSet;
import java.util.Set;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import patrykdadas.Utils;

public class AirlineFilter {

    private final SelenideElement filterButton = $(".filter-button.airlines");

    private final SelenideElement mainDiv = $("#mCSB_4_container");

    private final ElementsCollection options = mainDiv.$$(".airline-checkbox");

    private final SelenideElement showAllAirlines = options.first();

    public AirlineFilter() {
        filterButton.click();
    }

    public void clearAll() {
        if ( !showAllAirlines.find("input").isSelected() ) {
            showAllAirlines.click();
        }
        showAllAirlines.click();
    }

    public void selectAirlineWithName(String name) {
        options.find(Condition.text(name)).click();
    }

    public void selectRandomAirline() {
        clearAll();
        options.get(Utils.getNumberFromRange(1, options.size() - 1)).click();
    }

    public Set<String> getSelectedOptions() {
        Set<String> selectedOptions = new HashSet<>();
        for (SelenideElement option : options) {
            if ( option.$("input").isSelected() ) {
                selectedOptions.add(option.$("span").getText());
            }
        }
        return selectedOptions;
    }

    public void hide() {
        filterButton.click();
    }
}
