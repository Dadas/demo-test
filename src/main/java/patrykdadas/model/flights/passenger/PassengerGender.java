package patrykdadas.model.flights.passenger;

public enum PassengerGender {
    MALE, FEMALE
}
