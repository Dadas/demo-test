package patrykdadas.model.flights.passenger;

public enum PassengerType {
    ADULT, CHILD, INFANT
}
