package patrykdadas.model.flights.passenger;

import java.time.LocalDate;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import patrykdadas.Utils;

public class PassengerSection {

    private final SelenideElement monthOfBirthdayInput;

    private final SelenideElement frequentFlyerAirlineSelector;

    private final SelenideElement frequentFlyerNumberInput;

    private final SelenideElement baggageSelector;

    private final SelenideElement mealSelector;

    private final SelenideElement section;

    private final SelenideElement firstNameInput;

    private final SelenideElement lastNameInput;

    private final SelenideElement dayOfBirthdayInput;

    private final SelenideElement yearOfBirthdayInput;

    private final SelenideElement genderSelect;

    private final PassengerType passengerType;

    private final SelenideElement baggageOutboundSelector;

    private final SelenideElement baggageInboundSelector;

    public PassengerSection(SelenideElement sectionElement) {
        section = sectionElement;
        firstNameInput = section.find(".input-firstname");
        lastNameInput = section.find(".input-lastname");
        dayOfBirthdayInput = section.find(".day-input");
        monthOfBirthdayInput = section.find(".month-input");
        yearOfBirthdayInput = section.find(".year-input");
        genderSelect = section.find("select");
        passengerType = PassengerType.valueOf(section.parent().find("input").getValue());
        mealSelector = section.find("select[ng-model='passenger.meal'");
        baggageSelector = section.find("select[ng-model='passenger.baggage_option_code'");
        baggageOutboundSelector = section.find("select[ng-model='passenger.baggage_option_code_outbound'");
        baggageInboundSelector = section.find("select[ng-model='passenger.baggage_option_code_inbound'");
        frequentFlyerAirlineSelector = section.find("select[ng-model='passenger.frequent_flyer_number.airline_code']");
        frequentFlyerNumberInput = section.find("input[ng-value='passenger.frequent_flyer_number.number'");
    }

    public void fillDefaultData(PassengerData passenger) {
        firstNameInput.setValue(passenger.getFirstName());
        lastNameInput.setValue(passenger.getLastName());
        chooseGender(passenger.getGender());
        fillBirthdayDateInput(passenger.getBirthDate());
    }

    public boolean isAdultPassenger() {
        return passengerType.equals(PassengerType.ADULT);
    }

    public boolean isChildPassenger() {
        return passengerType.equals(PassengerType.CHILD);
    }

    public boolean isInfantPassenger() {
        return passengerType.equals(PassengerType.INFANT);
    }

    private void chooseGender(PassengerGender gender) {
        genderSelect.selectOptionByValue(gender.name().toLowerCase());
    }

    private void fillBirthdayDateInput(LocalDate date) {
        dayOfBirthdayInput.setValue(String.valueOf(date.getDayOfMonth())).pressTab();
        monthOfBirthdayInput.setValue(String.valueOf(date.getMonthValue())).pressTab();
        yearOfBirthdayInput.setValue(String.valueOf(date.getYear())).pressTab();
    }

    public void selectRandomBaggage() {
        showMoreOptions();
        if ( baggageSelector.isDisplayed() ) {
            Utils.chooseRandomValueInSelect(baggageSelector);
        } else {
            Utils.chooseRandomValueInSelect(baggageInboundSelector);
            Utils.chooseRandomValueInSelect(baggageOutboundSelector);
        }
    }

    public void selectRandomMeal() {
        showMoreOptions();
        Utils.chooseRandomValueInSelect(mealSelector);
    }

    public void selectRandomFrequentyFlyer() {
        showMoreOptions();
        Utils.chooseRandomValueInSelect(frequentFlyerAirlineSelector);
        frequentFlyerNumberInput.setValue(String.valueOf(Utils.getNumberFromRange(100000000, 999999999)));
    }

    public void showMoreOptions() {
        if ( section.find(".more-options").is(Condition.disappear) ) {
            section.find("a.show-more-options").click();
        }
    }
}
