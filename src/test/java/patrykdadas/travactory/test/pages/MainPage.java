package patrykdadas.travactory.test.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import java.time.LocalDate;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import patrykdadas.Utils;

public class MainPage extends BasePage {

    private final SelenideElement flightFromInput = $("#flight-from-main-search-box-f");

    private final SelenideElement flightFromAutocomplete = $("#ui-id-20");

    private final SelenideElement flightToInput = $("#flight-to-main-search-box-f");

    private final SelenideElement flightToAutocomplete = $("#ui-id-21");

    private final SelenideElement mainPageSearchBox = $("#search-main-search-box");

    private final SelenideElement departureDate = $(".departure-date");

    private final SelenideElement adultPassengersUl = $("#flight-passenger-type-0-main-search-box-f-menu");

    private final SelenideElement adultPassengersNumbersExpandButton = $("#flight-passenger-type-0-main-search-box-f-button");

    private final SelenideElement childPassengersUl = $("#flight-passenger-type-1-main-search-box-f-menu");

    private final SelenideElement childPassengersNumbersExpandButton = $("#flight-passenger-type-1-main-search-box-f-button");

    private final SelenideElement babyPassengersUl = $("#flight-passenger-type-2-main-search-box-f-menu");

    private final SelenideElement babyPassengersNumbersExpandButton = $("#flight-passenger-type-2-main-search-box-f-button");

    private final SelenideElement advancedSearchLink = $$(".advanced-search-toggle").find(Condition.visible);

    public void open() {
        Selenide.open("https://flighttix.pl/");
    }

    public void flightFrom(String from) {
        flightFromInput.setValue(from);
        flightFromAutocomplete.shouldBe(Condition.visible);
        flightFromInput.pressEnter();
    }

    public void flightTo(String to) {
        flightToInput.setValue(to);
        flightToAutocomplete.shouldBe(Condition.visible);
        flightToInput.pressEnter();
    }

    public void departureDate(LocalDate date) {
        departureDate.click();
        Utils.selectDateFromDatapicker(date);
    }

    public void returnDate(LocalDate date) {
        Utils.selectDateFromDatapicker(date);
    }

    public void chooseAdultPassengers(int numberOfPassengers) {
        adultPassengersNumbersExpandButton.click();
        Utils.selectValueFromUlList(String.valueOf(numberOfPassengers), adultPassengersUl);
    }

    public void chooseChildPassengers(int numberOfPassengers) {
        childPassengersNumbersExpandButton.click();
        Utils.selectValueFromUlList(String.valueOf(numberOfPassengers), childPassengersUl);
    }

    public void chooseInfantPassengers(int numberOfPassengers) {
        babyPassengersNumbersExpandButton.click();
        Utils.selectValueFromUlList(String.valueOf(numberOfPassengers), babyPassengersUl);
    }

    public void openAdvancedSearch() {
        advancedSearchLink.click();
    }

    @Override
    public void shouldBeLoaded() {
        mainPageSearchBox.shouldBe(Condition.visible);
    }
}
