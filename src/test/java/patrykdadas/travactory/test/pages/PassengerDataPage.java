package patrykdadas.travactory.test.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import patrykdadas.model.flights.booking.Address;
import patrykdadas.model.flights.booking.RandomAddress;
import patrykdadas.model.flights.passenger.PassengerSection;
import patrykdadas.model.flights.passenger.RandomPassenger;

public class PassengerDataPage extends BasePage {

    private final SelenideElement passengerDataForm = $("#passengers-details");

    private final SelenideElement moreButton = $(".section-submit").$("button");

    private final ElementsCollection passengersSectionList = $$(".section.section-passenger");

    public void fillAllWithRandomData() {
        for (SelenideElement section : passengersSectionList) {
            PassengerSection passengerSection = new PassengerSection(section);
            if ( passengerSection.isInfantPassenger() ) {
                passengerSection.fillDefaultData(RandomPassenger.newInfant());
            } else {
                if ( passengerSection.isAdultPassenger() ) {
                    passengerSection.fillDefaultData(RandomPassenger.newAdult());
                } else if ( passengerSection.isChildPassenger() ) {
                    passengerSection.fillDefaultData(RandomPassenger.newChild());
                }
                passengerSection.selectRandomBaggage();
            }
            passengerSection.selectRandomFrequentyFlyer();
            passengerSection.selectRandomMeal();
        }
        fillAddress(RandomAddress.get());

    }

    public void fillAddress(Address address) {
        SelenideElement streetNameInput = $$("input[ng-model='address.street'].input-street").find(Condition.visible);
        SelenideElement buildingNumber = $$("input[ng-model='address.number'].input-street-number-right").find(Condition.visible);
        SelenideElement postalCodeInput = $$("input[ng-model='address.postal_code'].input-postal-code").find(Condition.visible);
        SelenideElement cityInput = $$("input[ng-model='address.city'].input-city").find(Condition.visible);
        SelenideElement bookerPhone = $$("#main_booker_phone").find(Condition.visible);
        SelenideElement bookerEmail = $$("#booker_contact_details_email").find(Condition.visible);
        streetNameInput.setValue(address.getStreetName());
        buildingNumber.setValue(address.getBuildingNumber());
        postalCodeInput.setValue(address.getPostalCode());
        cityInput.setValue(address.getCity());
        bookerPhone.setValue(address.getPhoneNumber());
        bookerEmail.setValue(address.getEmail());
    }

    public void clickMoreButton() {
        moreButton.click();
    }

    @Override
    public void shouldBeLoaded() {
        waitForLoader();
        passengerDataForm.is(Condition.visible);
        passengersSectionList.shouldHave(CollectionCondition.sizeGreaterThan(0));
    }
}
