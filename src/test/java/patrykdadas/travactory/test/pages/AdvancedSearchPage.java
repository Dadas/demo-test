package patrykdadas.travactory.test.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

public class AdvancedSearchPage extends BasePage {

    private final SelenideElement advancedSearchForm = $("#form-search-flights-params-left-panel-search-box-f");

    private final SelenideElement hideAdvancedOptionsLink = $(".form-submit-wrap").$$("a").last();

    private final SelenideElement hasAdvancedOptionsInput = $("#has_advanced_params");

    private final SelenideElement searchButton = $("#form-search-flights-params-left-panel-search-box-f").findAll("button").filter(Condition.visible).first();

    public void hideAdvancedOptions() {
        if ( Boolean.valueOf(hasAdvancedOptionsInput.getValue()) ) {
            hideAdvancedOptionsLink.click();
        }
        $$(".advanced-search").filter(Condition.visible).shouldHaveSize(0);
        $$(".advanced-search-location").filter(Condition.visible).shouldHaveSize(0);
    }

    public void clickSearch() {
        searchButton.click();
    }

    @Override
    public void shouldBeLoaded() {
        waitForLoader();
        advancedSearchForm.shouldBe(Condition.visible);
    }
}
