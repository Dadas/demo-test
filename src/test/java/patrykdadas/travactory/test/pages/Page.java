package patrykdadas.travactory.test.pages;

public interface Page {
    void shouldBeLoaded();
}
