package patrykdadas.travactory.test.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import patrykdadas.Utils;

public class ExtraProductPage extends BasePage {

    private final SelenideElement extraProductsDiv = $("#extra-products-details");

    private final ElementsCollection insuranceProductsList = $$(".columns.small-12.ng-scope");

    private final SelenideElement touristInsuranceCheckbox = $$(".checkbox-btn-wrap").get(0);

    private final SelenideElement sendSmsCheckbox = $$(".checkbox-btn-wrap").get(1);

    private final ElementsCollection serviceList = $(".row.comparison-table.title-columns").$$("div");

    public void selectTouristInsurance() {
        touristInsuranceCheckbox.click();
    }

    public void selectSendSms() {
        sendSmsCheckbox.click();
    }

    public void selectRandomService() {
        serviceList.get(Utils.getNumberFromRange(0, serviceList.size() - 1)).click();
    }

    public void selectRandomInsuranceProduct() {
        insuranceProductsList.get(Utils.getNumberFromRange(0, insuranceProductsList.size() - 1)).click();
    }

    @Override
    public void shouldBeLoaded() {
        waitForLoader();
        extraProductsDiv.shouldBe(Condition.visible);
    }
}
