package patrykdadas.travactory.test.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.Set;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import patrykdadas.model.flights.results.ResultRow;
import patrykdadas.model.flights.results.filters.AirlineFilter;

public class ResultsPage extends BasePage {

    private final SelenideElement resultsHeader = $("#search-results-header");

    private final ElementsCollection resultItems = $$(".search-result-item");

    private final ElementsCollection additionalInfo = $$(".important-information");

    private final SelenideElement emptyResultsDiv = $("#empty-results");

    public void openFirstResult() {
        ResultRow resultRow = new ResultRow(resultItems.first());
        resultRow.expand();
    }

    public void bookFirstResult() {
        ResultRow resultRow = new ResultRow(resultItems.first());
        resultRow.clickBookButton();
    }

    public void additionalInfoIsExpanded() {
        additionalInfo.filterBy(Condition.visible).shouldHaveSize(1);
    }

    public void containsAtLeastOneResult() {
        resultItems.shouldHave(CollectionCondition.sizeGreaterThan(0));
    }

    public void hasNoResults() {
        resultItems.shouldHaveSize(0);
        emptyResultsDiv.shouldBe(Condition.visible);
    }

    public void filterResultsByRandomAirline() {
        AirlineFilter airlineFilter = new AirlineFilter();
        airlineFilter.selectRandomAirline();
        assertThat(airlineFilter.getSelectedOptions()).hasSize(1).as("Only one airline should be selected");
        airlineFilter.hide();
        waitForLoader();
    }

    public void verifyAirlineFilter() {
        Set<String> airlinesInResults = new HashSet<>();
        for (SelenideElement resultElement : resultItems) {
            airlinesInResults.add(new ResultRow(resultElement).getAirlaneName());
        }
        AirlineFilter airlineFilter = new AirlineFilter();
        Set<String> airlinesInFilter = airlineFilter.getSelectedOptions();
        airlineFilter.hide();
        waitForLoader();

        assertThat(airlinesInFilter).containsAll(airlinesInResults);
    }

    @Override
    public void shouldBeLoaded() {
        waitForLoader();
        resultsHeader.is(Condition.visible);
    }

    public void filterResultsByEmptyAirlinesList() {
        AirlineFilter airlineFilter = new AirlineFilter();
        airlineFilter.clearAll();
        airlineFilter.hide();
    }

    public void selectAirlineInFilter(String name) {
        AirlineFilter airlineFilter = new AirlineFilter();
        airlineFilter.selectAirlineWithName(name);
        airlineFilter.hide();
        waitForLoader();
    }
}

