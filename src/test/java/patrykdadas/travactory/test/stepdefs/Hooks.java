package patrykdadas.travactory.test.stepdefs;

import java.io.FileInputStream;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.logevents.SelenideLogger;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.qameta.allure.Allure;
import io.qameta.allure.selenide.AllureSelenide;

public class Hooks {
    private static boolean ran = false;

    @After
    public void afterScenario(Scenario scenario) {
        if ( scenario.isFailed() ) {
            try {
                Allure.addAttachment("Error - Screenshot", new FileInputStream(Screenshots.takeScreenShotAsFile()));
            } catch (Exception ignored) {
            }
        }
    }

    @Before
    public void beforeAll() {
        if ( !ran ) {
            SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
            ran = true;
        }
    }
}
