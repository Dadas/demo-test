package patrykdadas.travactory.test.stepdefs;

import java.util.List;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import patrykdadas.travactory.test.pages.ResultsPage;

public class ResultPageStepdefs {

    @Then("^Results page is opened$")
    public void resultPageIsOpened() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.shouldBeLoaded();
    }

    @Then("^I click on first result")
    public void clickOnFirstResult() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.openFirstResult();
    }

    @Then("^Additional data is expanded$")
    public void additionalDataIsExpanded() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.additionalInfoIsExpanded();
    }

    @When("^I click on first book button$")
    public void clickOnFirstBookButton() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.bookFirstResult();
    }

    @And("^Has at least one result item$")
    public void hasAtLeastOneResultItem() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.containsAtLeastOneResult();
    }

    @When("^I filter results by random airline name$")
    public void iFilterResulstByRandomAirlineName() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.filterResultsByRandomAirline();
    }

    @When("^I filter results by empty airlines list$")
    public void iFilterResulstByEmptyAirlinesList() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.filterResultsByEmptyAirlinesList();
    }

    @Then("^Results contains only flights selected airlines$")
    public void resultsContainsOnlyFlightsSelectedAirline() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.verifyAirlineFilter();
    }

    @Then("^Results page has no results$")
    public void resultsPageHasNoResults() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.hasNoResults();
    }

    @When("^I filter results by airlines:$")
    public void iFilterResultsByAirlines(DataTable airlinesTable) {
        List<String> airlines = airlinesTable.asList(String.class);
        ResultsPage resultsPage = new ResultsPage();

        for (String airline : airlines) {
            resultsPage.selectAirlineInFilter(airline);
        }
    }
}
