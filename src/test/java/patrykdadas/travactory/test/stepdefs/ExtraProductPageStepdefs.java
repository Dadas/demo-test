package patrykdadas.travactory.test.stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import patrykdadas.travactory.test.pages.ExtraProductPage;

public class ExtraProductPageStepdefs {
    @Then("^Extra product page is loaded$")
    public void extraProductPageIsLoaded() {
        ExtraProductPage extraProductPage = new ExtraProductPage();
        extraProductPage.shouldBeLoaded();
    }

    @Then("^I choose random insurance product$")
    public void chooseRandomInsuranceProduct() {
        ExtraProductPage extraProductPage = new ExtraProductPage();
        extraProductPage.selectRandomInsuranceProduct();
    }

    @Then("^I choose tourist insurance$")
    public void chooseTouristInsurance() {
        ExtraProductPage extraProductPage = new ExtraProductPage();
        extraProductPage.selectTouristInsurance();
    }

    @And("^I choose send sms$")
    public void chooseSendSms() {
        ExtraProductPage extraProductPage = new ExtraProductPage();
        extraProductPage.selectSendSms();
    }

    @And("^I choose random service$")
    public void chooseRandomService() {
        ExtraProductPage extraProductPage = new ExtraProductPage();
        extraProductPage.selectRandomService();
    }
}
