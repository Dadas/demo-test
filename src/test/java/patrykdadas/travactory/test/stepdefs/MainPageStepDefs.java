package patrykdadas.travactory.test.stepdefs;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import patrykdadas.travactory.test.pages.MainPage;

public class MainPageStepDefs {
    private LocalDate departureDate;

    @Given("^Open main page$")
    public void openMainPage() {
        MainPage mainPage = new MainPage();
        mainPage.open();
    }

    @Then("^Main page is opened$")
    public void mainPageIsOpened() {
        MainPage mainPage = new MainPage();
        mainPage.shouldBeLoaded();
    }

    @And("^I set \"([^\"]*)\" as flight from$")
    public void setFlightFrom(String from) {
        MainPage mainPage = new MainPage();
        mainPage.flightFrom(from);
    }

    @And("^I set \"([^\"]*)\" as flight to$")
    public void setFlightTo(String to) {
        MainPage mainPage = new MainPage();
        mainPage.flightTo(to);
    }

    @And("^I choose departure date plus (\\d+) days from now$")
    public void chooseDepartureDatePlusDaysFromNow(int number) {
        MainPage mainPage = new MainPage();
        LocalDate date = LocalDate.now();
        date = date.plus(number, ChronoUnit.DAYS);
        mainPage.departureDate(date);
        departureDate = date;
    }

    @And("^I choose return date plus (\\d+) days from departure date$")
    public void chooseReturnDatePlusDaysFromDepartureDate(int number) {
        MainPage mainPage = new MainPage();
        LocalDate returnDate = departureDate.plus(number, ChronoUnit.DAYS);
        mainPage.returnDate(returnDate);
    }

    @And("^I choose (\\d+) as number of adult passengers$")
    public void chooseNumberOfAdultPassengers(int number) {
        MainPage mainPage = new MainPage();
        mainPage.chooseAdultPassengers(number);
    }

    @And("^I choose (\\d+) as number of child passengers$")
    public void chooseNumberOfChildPassengers(int number) {
        MainPage mainPage = new MainPage();
        mainPage.chooseChildPassengers(number);
    }

    @And("^I choose (\\d+) as number of infant passengers$")
    public void chooseNumberOfInfantPassengers(int number) {
        MainPage mainPage = new MainPage();
        mainPage.chooseInfantPassengers(number);
    }

    @And("^I click advanced search$")
    public void clickAdvancedSearch() {
        MainPage mainPage = new MainPage();
        mainPage.openAdvancedSearch();
    }
}
