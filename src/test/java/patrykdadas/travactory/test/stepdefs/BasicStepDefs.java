package patrykdadas.travactory.test.stepdefs;

import static com.codeborne.selenide.Selenide.$$;

import com.codeborne.selenide.Condition;

import cucumber.api.java.en.And;

public class BasicStepDefs {

    @And("The page does not contain any errors")
    public void thePageDoesNotContainAnyErrors() {
        $$(".error ").filter(Condition.visible).shouldHaveSize(0);
    }
}
