Feature: Results airline filter test

  Scenario: Open main page
    Given Open main page
    Then Main page is opened
    And The page does not contain any errors

  Scenario: Fill and open Advanced Search
    Given Main page is opened
    When I set "AMS" as flight from
    And I set "MADRYT" as flight to
    And I choose departure date plus 60 days from now
    And I choose return date plus 5 days from departure date
    And I choose 1 as number of adult passengers
    And I click advanced search
    Then Advanced search is opened
    And The page does not contain any errors

  Scenario: Search in Adavanced view
    Given Advanced search is opened
    When I hide advanced options
    And I click search button on advanced search
    Then Results page is opened
    And Has at least one result item

  Scenario: Filter results by random airline
    Given Results page is opened
    When I filter results by random airline name
    Then Results contains only flights selected airlines

  Scenario: Empty results for empty airline filter
    Given Results page is opened
    When I filter results by empty airlines list
    Then Results page has no results

  Scenario: Filter results by specific airlines
    Given Results page is opened
    When I filter results by airlines:
      | Air Europa |
      | Iberia     |
      | Swiss      |
    Then Results contains only flights selected airlines


