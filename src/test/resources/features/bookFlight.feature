Feature:  Book flight

  Scenario: Open main page
    Given Open main page
    Then Main page is opened
    And The page does not contain any errors

  Scenario: Fill and open Advanced Search
    Given Main page is opened
    When I set "AMS" as flight from
    And I set "MADRYT" as flight to
    And I choose departure date plus 60 days from now
    And I choose return date plus 5 days from departure date
    And I choose 1 as number of adult passengers
    And I choose 1 as number of child passengers
    And I choose 1 as number of infant passengers
    And I click advanced search
    Then Advanced search is opened
    And The page does not contain any errors

  Scenario: Search in Adavanced view
    Given Advanced search is opened
    When I hide advanced options
    And I click search button on advanced search
    Then Results page is opened
    And Has at least one result item

  Scenario: Open first result
    Given Results page is opened
    When I click on first result
    Then Additional data is expanded
    And The page does not contain any errors

  Scenario: Reserve first result
    Given Additional data is expanded
    When I click on first book button
    Then Passenger data page is loaded
    And The page does not contain any errors

  Scenario: Fill passenger data page with random data
    Given Passenger data page is loaded
    When I fill all data with random values
    And I click on More button
    Then Extra product page is loaded
    And The page does not contain any errors

  Scenario: Choose extra products
    Given Extra product page is loaded
    When I choose random insurance product
    And I choose tourist insurance
    And I choose send sms
    And I choose random service
    Then The page does not contain any errors